# Motorola Moto Camera 2 (logan): List 8

<center><img src="https://telegra.ph/file/cf43da1a6481e7c094ff6.jpg"/></center>

Compatible Devices:

- Moto G⁸ Plus (doha)
- Moto G Power (sofia)
- Moto G Stylus (sofiap)
- Moto G⁸ Power (sofiar)
- Moto G⁸ (rav)
- Moto G Pro (sofiap)
- Moto G Fast (rav)
- Motorola One Hyper (def)
- Motorola One Vision Plus (doha)
- Motorola Razr (olson)


**How to add it in your tree**

To clone:

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_logan.git -b eleven-arm64 packages/apps/MotCamera2`

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_overlay.git -b ten packages/apps/MotCamera2-overlay`

`git clone https://gitlab.com/NemesisDevelopers/motorola/motorola_motosignatureapp.git -b eleven packages/apps/MotoSignatureApp`

Add this in your dependencies:

```
 {
   "repository": "motorola_camera2_logan",
   "target_path": "packages/apps/MotCamera2",
   "branch": "eleven-arm64",
   "remote": "moto-camera"
 }
```
Add this in your device.mk or common.mk:

```
# Moto Camera 2
PRODUCT_PACKAGES += \
    MotCamera2
```

# [Download & info](https://telegra.ph/Moto-Camera-2-List-N8-05-09)


 Copyright © 2020-2021 Nemesis Team
